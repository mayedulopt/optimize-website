﻿using CMS.Dto;
using CMS.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace CMS.UI
{
    public static class SessionHelper
    {

        public static UserResourceDto UserProfile
        {
            get { return GetFromSession<UserResourceDto>("UserProfile"); }
            set { SetInSession("UserProfile", value); }
        }

        

        private static object GetFromSession(string key)
        {
            return HttpContext.Current.Session[key];
        }
        private static T GetFromSession<T>(string key)
        {
            return (T)HttpContext.Current.Session[key];
        }
        private static void SetInSession(string key, object value)
        {
            HttpContext.Current.Session[key] = value;
        }


    }
}