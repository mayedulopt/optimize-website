﻿using System.Web;
using System.Web.Optimization;

namespace Optimize
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/js/core.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/script").Include(
                      "~/Content/js/script.js"));
           
            bundles.Add(new StyleBundle("~/Content/css-all").Include(
                      "~/Content/css/bootstrap.css",
                      "~/Content/css/fonts.css",
                      "~/Content/css/style.css"));
           
        }
    }
}
