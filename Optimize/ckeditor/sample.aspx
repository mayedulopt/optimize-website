﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sample.aspx.vb" Inherits="Admin_ckeditor_sample" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="ckeditor.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <label>Details:</label>
        <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
        <script>

            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.

            CKEDITOR.replace('<%=txtDetails.ClientID %>',
                {
                    filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                    "extraPlugins": "imagebrowser,justify",
                    "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                }
            );

        </script>
    </div>
    </form>
</body>
</html>
