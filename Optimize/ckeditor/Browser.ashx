﻿<%@ WebHandler Language="VB" Class="Browser" %>

Imports System
Imports System.Web
Imports System.IO

Public Class Browser : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim filePaths As String() = Directory.GetFiles(context.Server.MapPath("~") & "\Admin\Content\UserFiles\")
        Dim retVal As new Stringbuilder()
        For Each File As String In filePaths
            Dim fileName As String = File.Substring(File.LastIndexOf("\") + 1)
            Dim imageFullPath = "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.IndexOf("/Admin/")) &  "/Admin/Content/UserFiles/" & fileName
            retVal.Append("{" & _
                          """image"": """ & imageFullPath & """," & _
                          """thumb"": """ & imageFullPath & """," & _
                          """folder"": ""Default""" & _
                             "},")
            
        Next
        context.Response.Write("[" & retVal.ToString.TrimEnd(New Char() {","}) & "]")
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class