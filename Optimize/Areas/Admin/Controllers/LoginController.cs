﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Dto;
using CMS.Facade;
using CMS.UI;

namespace Optimize.Areas.Admin.Controllers
{
    public class LoginController : Controller

    {
        AdminUsersFacade _login = new AdminUsersFacade();
        // GET: Admin/Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(AdminPanelLoginsDto dto)
        {
            UserResourceDto vm = new UserResourceDto();
            vm = _login.DoLogin(dto);
            if (vm.UserId > 0)
            {
                SessionHelper.UserProfile = vm;
            }
            
            return RedirectToAction("Index", "Home", new { area = "Admin" });
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }
    }
}