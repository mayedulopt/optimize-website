﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Facade;
using CMS.Dto;


namespace Optimize.Areas.Admin.Controllers
{
    public class ContactsController : Controller
    {
        ContactFacade _contacts = new ContactFacade();
        // GET: Admin/Contacts
        public ActionResult Index()
        {
            var data = _contacts.GetAllContacts();
            return View(data);
        }

        [HttpPost]
        public JsonResult SaveMessage(ContactDto dto)
        {
            var data = _contacts.SaveContactMsg(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long id)
        {
            var data = _contacts.LoadContact((long)id);
            return View(data);
        }

        public JsonResult RemoveMessage(long id)
        {
            var data = _contacts.DeleteHTML(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}