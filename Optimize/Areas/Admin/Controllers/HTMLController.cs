﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Optimize.Areas.Admin.Controllers
{
    public class HTMLController : Controller
    {
        HTMLFacade _html = new HTMLFacade();
        // GET: Admin/HTML
        public ActionResult Index()
        {
            var data = _html.GetAllHtmls();
            return View(data);
        }
        public ActionResult AddNew(long? id)
        {
            if (id != null)
            {
                var data = _html.LoadHTML((long)id);
                return View(data);
            }
            else
                return View();
        }
        [HttpPost]
        public JsonResult SaveHTML(HTMLDto dto)
        {
            var data = _html.SaveHTML(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemoveHTML(long id)
        {
            var data = _html.DeleteHTML(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long id)
        {
            var data = _html.LoadHTML((long)id);
            return View(data);
        }
       
    }
}