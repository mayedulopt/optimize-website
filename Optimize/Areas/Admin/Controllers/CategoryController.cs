﻿using CMS.Admin.Facade;
using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Optimize.Areas.Admin.Controllers
{
    public class CategoryController : Controller
    {
        CategoryFacade _category = new CategoryFacade();
        // GET: Admin/Category
        public ActionResult Index()
        {
            var data = _category.GetAllCategory();
            return View(data);
        }
        public ActionResult AddNew(long? listid)
        {
            if (listid != null)
            {
                var data = _category.LoadCategory((long)listid);
                return View(data);
            }
            else
                return View();
        }
        [HttpPost]
        public JsonResult SaveCategory(List1Dto dto)
        {
            var data = _category.SaveCategory(dto);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemoveCategory(long listid)
        {
            var data = _category.DeleteCategory(listid);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(long listid)
        {
            var data = _category.LoadCategory((long)listid);
            return View(data);
        }
    }
}