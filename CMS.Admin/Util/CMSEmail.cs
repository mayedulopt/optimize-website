﻿using CMS.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;

namespace CMS.Admin.Util
{
   public static class CMSEmail
    {
       public static ResponseDto Send(EmailDto dto)
       {
           ResponseDto response=new ResponseDto();
           
           try
           {
               //Create the msg object to be sent
               MailMessage msg = new MailMessage();
                //Add your email address to the recipients
                msg.To.Add(dto.ToAddress);
                //msg.CC.Add(dto.CCAddress);

                //Configure the address we are sending the mail from
                MailAddress address = new MailAddress("donot-reply@optimizeab.com");
                msg.From = address;
                msg.Subject = dto.Subject;
                msg.Body = dto.EmailBody;
                msg.IsBodyHtml = true;

                //SmtpClient client = new SmtpClient();
                //client.Host = "relay-hosting.secureserver.net";
                //client.Port = 25;


                SmtpClient client = new SmtpClient("optimizeab.com",25);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //client.EnableSsl = true;
                
                //Setup credentials to login to our sender email address ("UserName", "Password")
                client.UseDefaultCredentials = false;
                NetworkCredential credentials = new NetworkCredential("donot-reply@optimizeab.com", "se9R&3b1");
                client.Credentials = credentials;

                //Send the msg
                client.Send(msg);

                //Display some feedback to the user to let them know it was sent

                //Using Gmailer
                // GMailer.GmailUsername = "shaikhahaan@gmail.com";
                //GMailer.GmailPassword = "abbas123";

                //GMailer mailer = new GMailer();
                //mailer.ToEmail = dto.ToAddress;
                //mailer.Subject = dto.Subject;
                //mailer.Body = dto.EmailBody;
                //mailer.IsHtml = true;
                //mailer.Send();

                response.Success=true;
               response.Message = "Thank you for Your request. We will get back to you soon.";
           }
           catch (Exception ex)
           {
               response.Success = false;
               response.Message = "Something wrong! We will get back to you soon.";
           }
           return response;
       }
    }
}
