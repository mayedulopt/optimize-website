﻿using AutoMapper;
using CMS.Dto;
using CMS.DAL;


namespace CMS.AutoMaps
{
    public class CMSMappingProfile : Profile
    {
        protected override void Configure()
        {
            //base.Configure();

            CreateMap<Contact, ContactDto>();
            CreateMap<ContactDto, Contact>();

            CreateMap<AdminPanelLogin, AdminPanelLoginsDto>();
            CreateMap<AdminPanelLoginsDto, AdminPanelLogin>();

            CreateMap<List1, List1Dto>();
            CreateMap<List1Dto, List1>();

            CreateMap<HTML, HTMLDto>();
            CreateMap<HTMLDto, HTML>();

        }
    }
}