﻿using AutoMapper;
using CMS.DAL;
using CMS.Dto;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Admin.Facade
{
   public class HTMLFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        public List<HTMLDto> GetAllHtmls()
        {
            List<HTMLDto> listdata = new List<HTMLDto>();
            var data = _uow.GetSet<HTML>().ToList();
            listdata = Mapper.Map<List<HTMLDto>>(data);
            return listdata;
        }
        public ResponseDto SaveHTML(HTMLDto dto)
        {
            var entity = new HTML();
            ResponseDto response = new ResponseDto();
            try
            {

                if (dto.HtmlID != null && dto.HtmlID > 0)
                {
                    entity = _uow.GetById<HTML>((long)dto.HtmlID);
                    entity = Mapper.Map(dto, entity);
                    entity.LastUpdated = DateTime.Now;
                    _uow.Update<HTML>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Text Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<HTML>(dto);
                    entity.LastUpdated = DateTime.Now;
                    _uow.Add<HTML>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Text Have been Added!";
                }

            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }

        public HTMLDto LoadHTML(long id)
        {
            HTMLDto dto = new HTMLDto();
            var entity = _uow.GetById<HTML>(id);
            dto = Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteHTML(long id)
        {
            var entity = new HTML();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<HTML>(id);
            _uow.Remove<HTML>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Text Have Been Removed Successfully!";
            return response;
        }
    }
}
