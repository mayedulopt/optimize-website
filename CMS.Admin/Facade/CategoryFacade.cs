﻿using CMS.Dto;
using CMS.DAL;
using CMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace CMS.Admin.Facade
{
    public class CategoryFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        public List<List1Dto> GetAllCategory()
        {
            List<List1Dto> listdata = new List<List1Dto>();
            var data = _uow.GetSet<List1>().Where(i=>i.Status==true).ToList();
            listdata = Mapper.Map<List<List1Dto>>(data);
            return listdata;

        }
        public ResponseDto SaveCategory(List1Dto dto)
        {
            var entity = new List1();
            ResponseDto response = new ResponseDto();
            try
            {

                if (dto.ListID != null && dto.ListID > 0)
                {
                    entity = _uow.GetById<List1>((long)dto.ListID);
                    entity = Mapper.Map(dto, entity);
                    entity.LastUpdated = DateTime.Now;
                    _uow.Update<List1>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Category Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<List1>(dto);
                    entity.LastUpdated = DateTime.Now;
                    _uow.Add<List1>(entity);
                    _uow.Save();
                    response.Success = true;
                    response.Message = "Category Have been Added!";
                }
               
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
            }
            return response;
        }

        public List1Dto LoadCategory(long listid)
        {
            List1Dto dto = new List1Dto();
            var entity = _uow.GetById<List1>(listid);
            dto = Mapper.Map(entity,dto);
            return dto;
        }
        public ResponseDto DeleteCategory(long listid)
        {
            var entity = new List1();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<List1>(listid);
            entity.Status = false;
            entity.LastUpdated = DateTime.Now;
            _uow.Update<List1>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Category Have Been Removed Successfully!";
            return response;
        }
    }
}
