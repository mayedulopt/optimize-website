﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Facade;
using CMS.Dto;
using CMS.DAL;
using AutoMapper;
using CMS.Infrastructure;

namespace CMS.Facade
{
   public class AdminUsersFacade : BaseFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());
        //public ResponseDto SaveContactMsg(AdminPanelLoginsDto dto)
        //{
        //    var entity = new AdminPanelLogin();
        //    ResponseDto response = new ResponseDto();
        //    EmailDto email = new EmailDto();
            
        //    try
        //    {
                
        //        if (dto.UserID != null && dto.UserID > 0)
        //        {
        //            entity = _uow.GetSet<AdminPanelLogin>().FirstOrDefault(x => x.UserID == dto.UserID);
        //            entity = Mapper.Map(dto, entity);
        //            GenService.Update(entity);
        //            response.Success = true;
        //            response.Message = "Contact Message Edited Successfully";
        //        }
        //        else
        //        {
        //            entity = Mapper.Map<AdminPanelLogin>(dto);
        //            entity.CreationDate = DateTime.Now;
        //            GenService.Add(entity);
        //            response.Success = true;
        //            response.Message = "Thank you for Your request. We will get back to you soon";
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        response.Message = ex.ToString();
        //        //"Contact Message Save Failed";
        //    }
        //    GenService.SaveChanges();
            
        //    return response;
        //}

        public UserResourceDto DoLogin(AdminPanelLoginsDto dto)
        {
            UserResourceDto vm = new UserResourceDto();
            var users = _uow.GetSet<AdminPanelLogin>();
            var user = _uow.GetSet<AdminPanelLogin>().FirstOrDefault(x => x.UN_1 == dto.UN_1 && x.PS_1 == dto.PS_1);
            vm.UserId = user.UserID;
            vm.UserName = user.Title;
            vm.IsAdmin =  (user.Role=="Admin"?true:false);
           
            return vm;

        }
    }
}
