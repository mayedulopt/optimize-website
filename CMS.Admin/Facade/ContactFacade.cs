﻿using CMS.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Dto;
using CMS;
using AutoMapper;
using CMS.Admin.Util;
using CMS.DAL;
using CMS.Infrastructure;

namespace CMS.Facade
{
   public class ContactFacade : BaseFacade
    {
        private readonly CMSUnitOfWork _uow = new CMSUnitOfWork(new CMSEntities());

        public List<ContactDto> GetAllContacts()
        {
            List<ContactDto> listdata = new List<ContactDto>();
            var data = _uow.GetSet<Contact>().ToList();
            listdata = Mapper.Map<List<ContactDto>>(data);
            return listdata;
        }
        public ResponseDto SaveContactMsg(ContactDto dto)
        {
            var entity = new Contact();
            ResponseDto response = new ResponseDto();
            EmailDto email = new EmailDto();
            string htmlString = @"<html>
                      <body><div>
                      <p>Dear Admin,</p>
                      <p>We Have got the contact request from the following Person</p><br/>
                      <p>Name: " + dto.FullName + "</p><p>Phone: " + dto.Phone + "</p><p>Email: " + dto.Email + "</p><p>Subject: " + dto.Subject + "</p><p>Message: " + dto.Message + "</p><br/><p>Thank You!,<br>-Optimize Team</br></p></div></body></html>";

            try
            {
                //email.FromAdrress = dto.Email;
                //email.CCAddress = "mayedul04@yahoo.com";
                //email.ToAddress = "mayedul019@gmail.com";
                email.ToAddress = "info@optimizeab.com";
                email.Subject = "Contact Us Request";
                email.EmailBody = htmlString;
                if (dto.ContactId != null && dto.ContactId > 0)
                {
                    entity = _uow.GetById<Contact>((long)dto.ContactId);
                    entity = Mapper.Map(dto, entity);
                    _uow.Update<Contact>(entity);
                    response.Success = true;
                    response.Message = "Contact Message Edited Successfully";
                }
                else
                {
                    entity = Mapper.Map<Contact>(dto);
                    entity.ContactDate = DateTime.Now;
                   _uow.Add<Contact>(entity);
                    response.Success = true;
                    response.Message = "Thank you for Your request. We will get back to you soon";
                }
                _uow.Save();
                CMSEmail.Send(email);
            }

            catch (Exception ex)
            {
                response.Message = ex.ToString();
                    //"Contact Message Save Failed";
            }
            
            
            return response;
        }

        public ContactDto LoadContact(long id)
        {
            ContactDto dto = new ContactDto();
            var entity = _uow.GetById<Contact>(id);
            dto = Mapper.Map(entity, dto);
            return dto;
        }
        public ResponseDto DeleteHTML(long id)
        {
            var entity = new Contact();
            ResponseDto response = new ResponseDto();
            entity = _uow.GetById<Contact>(id);
            _uow.Remove<Contact>(entity);
            _uow.Save();
            response.Success = true;
            response.Message = "Contact Message Have Been Removed Successfully!";
            return response;
        }
    }
}
