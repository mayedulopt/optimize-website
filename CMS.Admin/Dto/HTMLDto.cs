﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CMS.Dto
{
   public class HTMLDto
    {
        public int HtmlID { get; set; }
        public string Title { get; set; }
        public string SmallDetails { get; set; }
        [AllowHtml]
        public string BigDetails { get; set; }
        public string SmallImage { get; set; }
        public string BigImage { get; set; }
        public string ImageAltText { get; set; }
        public string Link { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string LinkTitle { get; set; }
        public string VideoLink { get; set; }
        public string VideoCode { get; set; }
        public Nullable<short> SmallImageWidth { get; set; }
        public Nullable<short> SmallImageHeight { get; set; }
        public Nullable<short> BigImageWidth { get; set; }
        public Nullable<short> BigImageHeight { get; set; }
        public Nullable<short> VideoWidth { get; set; }
        public Nullable<short> VideoHeight { get; set; }
        public Nullable<int> MasterID { get; set; }
        public string Lang { get; set; }
    }
}
