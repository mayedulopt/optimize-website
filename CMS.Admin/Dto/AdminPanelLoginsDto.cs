﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class AdminPanelLoginsDto
    {
        public int UserID { get; set; }
        public string PS_1 { get; set; }
        public string UN_1 { get; set; }
        public string Title { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public Nullable<System.DateTime> CreationDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<bool> Status { get; set; }
    }
}
