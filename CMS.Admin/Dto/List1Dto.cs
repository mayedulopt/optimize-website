﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Dto
{
   public class List1Dto
    {
        public int ListID { get; set; }
        public string Title { get; set; }
        public string SmallDetails { get; set; }
        [DataType(DataType.MultilineText)]
        public string BigDetails { get; set; }
        public string SmallImage { get; set; }
        public string BigImage { get; set; }
        public string ImageAltText { get; set; }
        public Nullable<int> SortIndex { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string Link { get; set; }
        public Nullable<bool> Featured { get; set; }
        public Nullable<int> MasterID { get; set; }
        public string Lang { get; set; }
    }
}
